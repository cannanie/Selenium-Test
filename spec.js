describe('Protractor Demo App', function() {
  it('should have a title', function() {
    browser.get('http://' + process.env.HELLO_IP + '/');
    expect(browser.getTitle()).toEqual('Hello World!');
  });
});

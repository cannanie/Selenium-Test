exports.config = {
  framework: 'mocha',
  seleniumAddress: 'http://selenium:4444/wd/hub',
  specs: ['spec.js'],
  onPrepare: function() {
   browser.ignoreSynchronization = true;
  },
  mochaOpts: {
    reporter: 'mocha-multi-reporters',
    timeout: 60000,
    reporterOptions: {
      'reporterEnabled': 'spec, mocha-junit-reporter',
      'mochaJunitReporterReporterOptions': {
        'mochaFile': 'aat-results.xml'
      }
    }
  },
}
